<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\Ui;

use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Model\Ui\TokenUiComponentInterfaceFactory;
use Magento\Vault\Model\Ui\TokenUiComponentProviderInterface;
use PeachPayments\Hosted\Model\Ui\ConfigProvider;

class TokenUiComponentProvider implements TokenUiComponentProviderInterface
{
    /**
     * @var TokenUiComponentInterfaceFactory
     */
    private $tokenComponentFactory;

    /**
     * @param TokenUiComponentInterfaceFactory $tokenComponentFactory
     */
    public function __construct(TokenUiComponentInterfaceFactory $tokenComponentFactory)
    {
        $this->tokenComponentFactory = $tokenComponentFactory;
    }

    /**
     * @inheritDoc
     */
    public function getComponentForToken(PaymentTokenInterface $paymentToken)
    {
        $jsonDetails = json_decode($paymentToken->getTokenDetails() ?: '{}', true);
        return $this->tokenComponentFactory->create(
            [
                'config' => [
                    'code' => ConfigProvider::CODE_CC_VAULT,
                    TokenUiComponentProviderInterface::COMPONENT_DETAILS => $jsonDetails,
                    TokenUiComponentProviderInterface::COMPONENT_PUBLIC_HASH => $paymentToken->getPublicHash()
                ],
                'name' => 'PeachPayments_Hosted/js/view/payment/method-renderer/server-to-server-vault'
            ]
        );
    }
}
