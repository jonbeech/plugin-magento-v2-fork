<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

/**
 * Class \PeachPayments\Hosted\Model\System\Config\Source\Method
 */
namespace PeachPayments\Hosted\Model\System\Config\Source;

use PeachPayments\Hosted\Model\Method\ApplePay;

class Method
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'CARD', 'label' => __('CARD')],
            ['value' => 'EFTSECURE', 'label' => __('EFTSECURE')],
            ['value' => 'MASTERPASS', 'label' => __('MASTERPASS')],
            ['value' => 'MOBICRED', 'label' => __('MOBICRED')],
            ['value' => 'MPESA', 'label' => __('MPESA')],
            ['value' => 'APLUS', 'label' => __('A+ CARD')],
            ['value' => ApplePay::PAYMENT_CODE, 'label' => __('ApplePay')],
            ['value' => 'PAYPAL', 'label' => __('PayPal')],
            ['value' => 'STITCHEFT', 'label' => __('StitchEFT')],
            ['value' => 'PAYFLEX', 'label' => __('Payflex')],
            ['value' => 'ZEROPAY', 'label' => __('ZeroPay')],
            ['value' => '1FORYOU', 'label' => __('1Voucher')],
        ];
    }
}
