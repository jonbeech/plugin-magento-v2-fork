<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\Api;

use Magento\Customer\Model\Session as CustomerSession;
use PeachPayments\Hosted\Api\CopyAndPayPrepareAddNewCartFormInterface;
use PeachPayments\Hosted\Gateway\Request\AuthDataBuilder;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;
use PeachPayments\Hosted\Gateway\Request\VaultDataBuilder;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Command\GetVaultGatewayTokensForCart;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;

class CopyAndPayPrepareAddNewCartForm implements CopyAndPayPrepareAddNewCartFormInterface
{
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var CurlClient
     */
    private $curlClient;
    /**
     * @var JsonSerializer
     */
    private $json;
    /**
     * @var GetVaultGatewayTokensForCart
     */
    private $gatewayTokensForCart;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var HttpTransferObject
     */
    private $httpTransferObject;

    /**
     * @param ConfigHelper $configHelper
     * @param CurlClient $curlClient
     * @param JsonSerializer $json
     * @param GetVaultGatewayTokensForCart $gatewayTokensForCart
     * @param CustomerSession $customerSession
     * @param HttpTransferObject $httpTransferObject
     */
    public function __construct(
        ConfigHelper $configHelper,
        CurlClient $curlClient,
        JsonSerializer $json,
        GetVaultGatewayTokensForCart $gatewayTokensForCart,
        CustomerSession $customerSession,
        HttpTransferObject $httpTransferObject
    ) {
        $this->configHelper = $configHelper;
        $this->curlClient = $curlClient;
        $this->json = $json;
        $this->gatewayTokensForCart = $gatewayTokensForCart;
        $this->customerSession = $customerSession;
        $this->httpTransferObject = $httpTransferObject;
    }

    /**
     * @inheritDoc
     */
    public function execute(): string
    {
        $transfer = $this->httpTransferObject->create(
            $this->configHelper->getCheckoutsUri(),
            'POST',
            [
                AuthDataBuilder::ID => $this->configHelper->getEntityId3DSecure(),
                PaymentDataBuilder::PAYMENT_TYPE => 'PA',
                PaymentDataBuilder::CURRENCY => 'ZAR',
                PaymentDataBuilder::AMOUNT => '1.00',
                VaultDataBuilder::VAULT_ON => true,
                VaultDataBuilder::MODE => 'INITIAL',
                VaultDataBuilder::TYPE => 'UNSCHEDULED',
                VaultDataBuilder::SOURCE => 'CIT'
            ]
        );

        try {
            $result = $this->curlClient->placeRequest($transfer);

            return $this->json->serialize(
                !isset($result['id']) ?
                    [] :
                    ['checkout_id' => $result['id']]
            );
        } catch (\Exception $e) {
            return $this->json->serialize([]);
        }
    }
}
