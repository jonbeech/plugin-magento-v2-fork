<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\Api;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface as QuoteMaskToQuoteId;
use PeachPayments\Hosted\Api\CopyAndPayPrepareInterface;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Gateway\Request\AuthDataBuilder;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;
use PeachPayments\Hosted\Gateway\Request\VaultDataBuilder;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Command\GetVaultGatewayTokensForCart as RegistrationIdsCommand;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;

/**
 * Prepare copy and pay checkout
 */
class CopyAndPayPrepare implements CopyAndPayPrepareInterface
{
    const REQUEST_SAVE_CARD_DETAILS = 'createRegistration';
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var CurlClient
     */
    private $curlClient;
    /**
     * @var QuoteMaskToQuoteId
     */
    private $quoteMaskedToId;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;
    /**
     * @var WebhooksFactory
     */
    private $webhooksFactory;
    /**
     * @var WebhooksResource
     */
    private $webhooksResource;
    /**
     * @var RegistrationIdsCommand
     */
    private $registrationIdsCommand;
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;
    /**
     * @var HttpTransferObject
     */
    private $httpTransferObject;

    /**
     * @param CartRepositoryInterface $cartRepository
     * @param ConfigHelper $configHelper
     * @param CurlClient $curlClient
     * @param QuoteMaskToQuoteId $quoteMaskedToId
     * @param CustomerSession $customerSession
     * @param ObjectManagerInterface $objectManager
     * @param JsonSerializer $jsonSerializer
     * @param WebhooksFactory $webhooksFactory
     * @param WebhooksResource $webhooksResource
     * @param RegistrationIdsCommand $registrationIdsCommand
     * @param RemoteAddress $remoteAddress
     * @param HttpTransferObject $httpTransferObject
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        ConfigHelper $configHelper,
        CurlClient $curlClient,
        QuoteMaskToQuoteId $quoteMaskedToId,
        CustomerSession $customerSession,
        ObjectManagerInterface $objectManager,
        JsonSerializer $jsonSerializer,
        WebhooksFactory $webhooksFactory,
        WebhooksResource $webhooksResource,
        RegistrationIdsCommand $registrationIdsCommand,
        RemoteAddress $remoteAddress,
        HttpTransferObject $httpTransferObject
    ) {
        $this->cartRepository = $cartRepository;
        $this->configHelper = $configHelper;
        $this->curlClient = $curlClient;
        $this->quoteMaskedToId = $quoteMaskedToId;
        $this->customerSession = $customerSession;
        $this->objectManager = $objectManager;
        $this->jsonSerializer = $jsonSerializer;
        $this->webhooksFactory = $webhooksFactory;
        $this->webhooksResource = $webhooksResource;
        $this->registrationIdsCommand = $registrationIdsCommand;
        $this->remoteAddress = $remoteAddress;
        $this->httpTransferObject = $httpTransferObject;
    }

    /**
     * @inheritDoc
     */
    public function execute(string $cartId): string
    {
        $result = [];
        if (!$this->configHelper->isEnabled()) {
            return $this->jsonSerializer->serialize($result);
        }

        if (!$this->customerSession->isLoggedIn()) {
            $cartId = $this->quoteMaskedToId->execute($cartId);
        }

        $cart = $this->cartRepository->get($cartId);
        $amount = number_format($cart->getGrandTotal(), 2, '.', '');
        $currency = $cart->getCurrency()->getQuoteCurrencyCode();
        $entityId3DSecure = $this->configHelper->getEntityId3DSecure();

        $requestData = [
            AuthDataBuilder::ID => $entityId3DSecure,
            PaymentDataBuilder::AMOUNT => $amount,
            PaymentDataBuilder::CURRENCY => $currency,
            PaymentDataBuilder::PAYMENT_TYPE => 'DB',
            VaultDataBuilder::MODE => 'INITIAL',
            VaultDataBuilder::TYPE => 'UNSCHEDULED',
            VaultDataBuilder::SOURCE => 'CIT'
        ];

        if ($this->cartHasSubscriptions($cart)) {
            $requestData[self::REQUEST_SAVE_CARD_DETAILS] = true;
            $requestData[VaultDataBuilder::MODE] = 'INITIAL';
            $requestData[VaultDataBuilder::TYPE] = 'RECURRING';
            $requestData[VaultDataBuilder::SOURCE] = 'CIT';
        }

        $registrations = $this->getRegistrationIdsRequestParams($cart);
        $requestData = array_merge($requestData, $registrations);

        $transfer = $this->httpTransferObject->create(
            $this->configHelper->getCheckoutsUri(),
            'POST',
            $requestData
        );

        $response = $this->curlClient->placeRequest($transfer);
        $result['checkout_id'] = $response['id'] ?? '';

        $cart->getPayment()->setAdditionalInformation(AuthorizationTrxIdHandler::KEY_TNX_ID, $result['checkout_id']);
        $this->cartRepository->save($cart);

        $this->prepareWebhook($result['checkout_id']);
        $cart->reserveOrderId();

        return $this->jsonSerializer->serialize(
            array_merge(
                [
                    'merchantTransactionId' => $cart->getReservedOrderId(),
                    'customParameters[PAYMENT_PLUGIN]' => 'MAGENTO',
                    'customer.ip' => $this->remoteAddress->getRemoteAddress(),
                    AuthDataBuilder::ID => $entityId3DSecure,
                    PaymentDataBuilder::AMOUNT => $amount,
                    PaymentDataBuilder::CURRENCY => $currency,
                    PaymentDataBuilder::PAYMENT_TYPE => 'DB',
                ],
                $result
            )
        );
    }

    /**
     * @param $cart CartInterface
     * @return bool
     */
    private function cartHasSubscriptions(CartInterface $cart): bool
    {
        try {
            $subsModuleEnabled = $this->configHelper->isModuleOutputEnabled('ParadoxLabs_Subscriptions');
            if ($subsModuleEnabled) {
                $quoteManager = $this->objectManager->get('ParadoxLabs\Subscriptions\Model\Service\QuoteManager');
                if ($quoteManager->quoteContainsSubscription($cart)) {
                    return true;
                }
            }
        } catch (\Exception $exception) {
            return false;
        }

        return false;
    }

    /**
     * @param string $checkoutId
     * @return void
     */
    private function prepareWebhook(string $checkoutId)
    {
        try {
            $webhook = $this->webhooksFactory->create();
            $this->webhooksResource->load($webhook, $checkoutId, 'checkout_id');
            if (!$webhook->getId()) {
                $webhook->setData('checkout_id', $checkoutId);
                $this->webhooksResource->save($webhook);
            }
        } catch (\Exception $e) {
        }
    }

    private function getRegistrationIdsRequestParams(CartInterface $cart): array
    {
        $params = [];
        $registrationIds = $this->registrationIdsCommand->execute($cart);
        if (empty($registrationIds)) {
            return $params;
        }

        foreach ($registrationIds as $key => $registrationId) {
            $params["registrations[$key].id"] = $registrationId;
        }

        $params[VaultDataBuilder::MODE] = 'REPEATED';
        $params[VaultDataBuilder::TYPE] = 'UNSCHEDULED';
        $params[VaultDataBuilder::SOURCE] = 'CIT';

        return $params;
    }
}
