<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Plugin;

use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Model\Ui\ConfigProvider;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;
use PeachPayments\Hosted\Gateway\Http\Client as HttpClient;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;

class DeletePaymentRegistrationPlugin
{
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var PeachPaymentsHelper
     */
    private $peachPaymentsHelper;
    /**
     * @var HttpClient
     */
    private $httpClient;
    /**
     * @var HttpTransferObject
     */
    private $httpTransferObject;

    /**
     * @param ConfigHelper $configHelper
     * @param PeachPaymentsHelper $peachPaymentsHelper
     * @param HttpClient $httpClient
     * @param HttpTransferObject $httpTransferObject
     */
    public function __construct(
        ConfigHelper $configHelper,
        PeachPaymentsHelper $peachPaymentsHelper,
        HttpClient $httpClient,
        HttpTransferObject $httpTransferObject
    ) {
        $this->configHelper = $configHelper;
        $this->peachPaymentsHelper = $peachPaymentsHelper;
        $this->httpClient = $httpClient;
        $this->httpTransferObject = $httpTransferObject;
    }

    /**
     * @param PaymentTokenRepositoryInterface $subject
     * @param PaymentTokenInterface $paymentToken
     * @return array
     */
    public function beforeDelete(
        PaymentTokenRepositoryInterface $subject,
        PaymentTokenInterface $paymentToken
    ): array {
        $token = $paymentToken->getGatewayToken();
        $paymentMethodCode = $paymentToken->getPaymentMethodCode();

        if ($paymentMethodCode == ConfigProvider::CODE) {
            $uri = $this->configHelper->getApiDeleteRegistrationUri($token, $this->peachPaymentsHelper->getEntityId());

            $transfer = $this->httpTransferObject->create($uri, 'DELETE');

            try {
                $this->httpClient->placeRequest($transfer);
            } catch (\Exception $exception) {
                return [$paymentToken];
            }
        }

        return [$paymentToken];
    }
}
