define(
    [
        'jquery',
        'Magento_Payment/js/view/payment/cc-form',
        'Magento_Vault/js/view/payment/vault-enabler',
        'Magento_Payment/js/model/credit-card-validation/credit-card-number-validator',
        'Magento_Ui/js/model/messageList',
        'mage/translate',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/action/place-order'
    ],
    function ($, Component, VaultEnabler, cardNumberValidator, messageList, $t, additionalValidators, placeOrderAction) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'PeachPayments_Hosted/payment/server-to-server-form',
            },

            initialize: function () {
                let self = this;

                self._super();
                this.vaultEnabler = new VaultEnabler();
                this.vaultEnabler.setPaymentCode(this.getVaultCode());
                return self;
            },

            getCode: function() {
                return 'peachpayments_server_to_server';
            },

            // validate: function () {
            //     return true;
            // },

            getData: function () {
                let data = this._super();
                data['additional_data']['payment_method_nonce'] = window.checkoutConfig.payment[this.getCode()].nonce;
                data['additional_data'] = _.extend(data['additional_data'], this.additionalData);
                this.vaultEnabler.visitAdditionalData(data);

                return data;
            },

            isVaultEnabled: function () {
                return this.vaultEnabler.isVaultEnabled();
            },

            getVaultCode: function () {
                return window.checkoutConfig.payment[this.getCode()].ccVaultCode;
            },

            getCcAvailableTypes: function () {
                let availableTypes = {};
                const types = window.checkoutConfig.payment[this.getCode()].availableTypes;
                _.each(types, function(obj) {
                    _.extend(availableTypes, obj);
                });

                return availableTypes;
            },

            isActive: function () {
                return window.checkoutConfig.payment[this.getCode()].isActive;
            },
        });
    }
);
