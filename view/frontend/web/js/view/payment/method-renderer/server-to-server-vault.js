/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */
/*browser:true*/
/*global define*/
define([
    'underscore',
    'Magento_Vault/js/view/payment/method-renderer/vault',
    'Magento_Checkout/js/model/quote'
], function (_, VaultComponent, quote) {
    'use strict';

    return VaultComponent.extend({
        defaults: {
            template: 'PeachPayments_Hosted/payment/vault/server-to-server-form'
        },

        /**
         * @returns {String}
         */
        getToken: function () {
            return this.publicHash;
        },

        /**
         * Get last 4 digits of card
         * @returns {String}
         */
        getMaskedCard: function () {
            return this.details['maskedCC'];
        },

        /**
         * Get expiration date
         * @returns {String}
         */
        getExpirationDate: function () {
            return this.details['expirationDate'];
        },

        /**
         * Get card type
         * @returns {String}
         */
        getCardType: function () {
            return this.details['type'];
        },

        /**
         * @returns {number}
         */
        isAvailable: function () {
            return quote.containsSubscription();
        }
    });
});
