/*
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

define(
  [
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/place-order',
    'Magento_Checkout/js/action/select-payment-method',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/payment/additional-validators',
    'mage/url',
  ],
  function (
    $,
    quote,
    urlBuilder,
    storage,
    customerData,
    Component,
    placeOrderAction,
    selectPaymentMethodAction,
    customer,
    checkoutData,
    additionalValidators,
    url
  ) {
    'use strict';

    var peachPaymentsLogos = {
        'peachpayments_hosted_masterpass': 'PeachPayments_Hosted/images/payment/scan-to-pay.svg',
        'peachpayments_hosted_mobicred'  : 'PeachPayments_Hosted/images/payment/mobicred.svg',
        'peachpayments_hosted_zeropay'   : 'PeachPayments_Hosted/images/payment/zeropay.svg',
        'peachpayments_hosted_payflex'   : 'PeachPayments_Hosted/images/payment/payflex.svg',
        'peachpayments_hosted_stitcheft' : 'PeachPayments_Hosted/images/payment/instanteft.svg',
        'peachpayments_hosted_eftsecure' : 'PeachPayments_Hosted/images/payment/eftsecure.svg',
        'peachpayments_hosted_mpesa'     : 'PeachPayments_Hosted/images/payment/mpesa.svg',
        'peachpayments_hosted_card'      : 'PeachPayments_Hosted/images/payment/visa-mastercard.svg',
        'peachpayments_hosted_aplus'     : 'PeachPayments_Hosted/images/payment/aplus.svg',
        'peachpayments_hosted_1foryou'   : 'PeachPayments_Hosted/images/payment/1foryou.svg'
    };

    return Component.extend({
      redirectAfterPlaceOrder: false,
      defaults: {
        template: 'PeachPayments_Hosted/payment/hosted'
      },
      peachPlaceOrder: function (data, event) {

        if (event) {
          event.preventDefault();
        }

        var self = this,
          placeOrder,
          emailValidationResult = customer.isLoggedIn(),
          loginFormSelector = 'form[data-role=email-with-possible-login]';

        if (!customer.isLoggedIn()) {
          $(loginFormSelector).validation();
          emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
        }

        if (emailValidationResult && this.validate() && additionalValidators.validate()) {
          this.isPlaceOrderActionAllowed(false);
          placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

          $.when(placeOrder).fail(function () {
            self.isPlaceOrderActionAllowed(true);
          }).done(this.afterPlaceOrder.bind(this));
          return true;
        }
        return false;
      },

      selectPaymentMethod: function () {
        selectPaymentMethodAction(this.getData());
        checkoutData.setSelectedPaymentMethod(this.item.method);
        return true;
      },

      afterPlaceOrder: function () {
        window.location.replace(url.build('pp-hosted/secure/redirect'));
      },

      /**
       * Get payment method Logo.
       */
      getLogo: function () {
          let logo = false,
              paymentCode = this.item.method;

          if (peachPaymentsLogos.hasOwnProperty(paymentCode)) {
              logo = require.toUrl(peachPaymentsLogos[paymentCode]);
          }

          return logo;
      }
    });
  }
);
