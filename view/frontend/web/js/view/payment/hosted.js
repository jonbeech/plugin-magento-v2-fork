/*
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        let availableHostedMethods = [
            'card',
            'eftsecure',
            'masterpass',
            'mobicred',
            'mpesa',
            'aplus',
            'stitcheft',
            'payflex',
            'zeropay',
            '1foryou'
        ];

        let storeBaseCurrency = window.checkoutConfig.totalsData.base_currency_code,
            isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

        (isSafari && availableHostedMethods.push('applepay'));
        ((storeBaseCurrency === 'USD') && availableHostedMethods.push('paypal'));

        for (let k = 0; k < availableHostedMethods.length; k++) {
            rendererList.push(
                {
                    type: 'peachpayments_hosted_' + availableHostedMethods[k],
                    component: 'PeachPayments_Hosted/js/view/payment/method-renderer/hosted'
                }
            );
        }

        return Component.extend({});
    }
);
