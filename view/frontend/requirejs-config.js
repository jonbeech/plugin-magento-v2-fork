var config = {
    map: {
        '*': {
            addNewPaymentCard: 'PeachPayments_Hosted/js/view/add-new-payment-card'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/model/quote': {
                'PeachPayments_Hosted/js/mixin/model/quote': true
            },
            'Magento_Checkout/js/view/payment/list': {
                'PeachPayments_Hosted/js/mixin/view/payment/list': true
            }
        }
    }
};
