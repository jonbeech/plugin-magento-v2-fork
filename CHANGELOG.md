# Peach Payments Magento payment extension changelog

## 2022.12.19 - version 1.2.6

- Enhancement - Added **New Card** via **My Account**.
- Enhancement - Added **Force Default Payment Method**.
- Fix - Minicart being cached on embedded card or saved card redirect.
- Fix - Embedded card form to pass transaction or order ID to Peach Payments.
- Fix - InstantEFT payments shown as card.

## 2022.11.14 - version 1.2.5

- Enhancement - Added billing fields on the embedded card payment form.
- Enhancement - Improved error logging.
- Enhancement - Updated PHP compatibility to 8.1 and added support for Magento 2.4.5.
- Fix - Transactions failing when buying subscription products.
- Fix - Subscription product + coupon totals calculations.
- Fix - Payment status stays pending after successful payment.
- Fix - Partial refund error.

## 2022.08.25 - version 1.2.4

- Feature - Added support for one-click checkout.
- Enhancement - Updated payment method logos.
- Fix - Removed billing state from initial payment requests.
- Fix - Added credential on file (COF) parameters in payment requests.
- Fix - Payment refund error.

## 2022.06.25 - version 1.2.3

- Enhancement - Updated payment method logos.
- Enhancement - Added logging.
- Fix - Refunds and other module errors.
- Fix - Added compatibility with PHP 7.3.

## 2022.04.11 - version 1.2.2

- Feature - Added integration into the ParadoxLabs Subscription plugin.
- Feature - Added webhook support when payments are done on the card widget.
- Enhancement - Added support for 3-D Secure 2.0.
- Fix - Applied new Peach Payments brand.
- Fix - Removed inactive payment methods.
- Fix - Fixed spelling errors.
- Fix - Used our Copy and Pay widget instead of the Magento native card form.
- Fix - Added logic to show the card widget only if a subscription product is in the cart.
- Fix - Fixed an error when viewing invoices for Masterpass payments.
- Fix - Made the recurring `EntityId` field optional.
