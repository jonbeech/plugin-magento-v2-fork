<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

/**
 * Class BlockRedirect
 *
 * @method getOrderId
 * @method getOrderIncrementId
 */

namespace PeachPayments\Hosted\Block;

use Exception;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order;
use PeachPayments\Hosted\Helper\Data as HelperData;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Model\Method\ApplePay;

class Redirect extends Template
{
    /**
     * @var HelperData
     */
    protected $helperData;
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;
    /**
     * @var SessionManagerInterface
     */
    private $checkoutSession;
    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * Redirect constructor.
     * @param Context $context
     * @param HelperData $helperData
     * @param RemoteAddress $remoteAddress
     * @param ConfigHelper $configHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        HelperData $helperData,
        RemoteAddress $remoteAddress,
        ConfigHelper $configHelper,
        array $data = []
    ) {
        $this->helperData = $helperData;
        $this->remoteAddress = $remoteAddress;
        $this->checkoutSession = $context->getSession();
        $this->configHelper = $configHelper;
        parent::__construct($context, $data);
    }

    /**
     * Redirect template
     */
    protected function _construct()
    {
        $this->setTemplate('PeachPayments_Hosted::peachpayments_hosted/redirect.phtml');
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->helperData->getCheckoutUrl();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getFormData(): array
    {
        return $this->helperData->signData($this->getUnsortedFormData());
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return ObjectManager::getInstance()->get('Magento\Checkout\Model\Session')->getLastRealOrder();
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getUnsortedFormData(): array
    {
        $order = $this->getOrder();
        $helper = $this->helperData;
        $amount = number_format(
            $order->getPayment()->getAmountOrdered(),
            2,
            '.',
            ''
        );

        $defaultPaymentMethod = [];
        $methodCode = strtoupper(
            str_replace(
                'peachpayments_hosted_',
                '',
                $order->getPayment()->getMethodInstance()->getCode()
            )
        );

        if ($methodCode != ApplePay::PAYMENT_CODE) {
            $defaultPaymentMethod = [
                'defaultPaymentMethod' => $methodCode
            ];
        }

        $billingStreet = (array) $order->getBillingAddress()->getStreet();
        array_push($billingStreet, 'N/A', 'N/A');
        list($billingStreetOne, $billingStreetTwo) = $billingStreet;

        $billingStreetOne = $order->getBillingAddress()->getCountryId() . ' ' . $billingStreetOne;

        return array_merge([
            'authentication.entityId' => $this->configHelper->getEntityId3DSecure(),
            'amount' => $amount,
            'paymentType' => 'DB',
            'currency' => $order->getOrderCurrencyCode(),
            'shopperResultUrl' => $this->getUrl('*/*/payment/'),
            'customParameters[PHPSESSID]' => $this->checkoutSession->getSessionId(),
            'merchantTransactionId' => $order->getIncrementId(),
            'plugin' => $helper->getPlatformName(),
            'forceDefaultMethod' => $this->configHelper->getIsForceDefaultMethod($methodCode),

            'customer.givenName' => $order->getBillingAddress()->getFirstname(),
            'customer.surname' => $order->getBillingAddress()->getLastname(),
            'customer.mobile' => $order->getBillingAddress()->getTelephone(),
            'customer.email' => $order->getBillingAddress()->getEmail(),
            'customer.status' => $order->getCustomerIsGuest() ? 'NEW' : 'EXISTING',
            'customer.ip' => $this->remoteAddress->getRemoteAddress(),

            'billing.street1' => $billingStreetOne,
            'billing.street2' => $billingStreetTwo,
            'billing.city' => $order->getBillingAddress()->getCity(),
            'billing.country' => $order->getBillingAddress()->getCountryId(),
            'billing.postcode' => $order->getBillingAddress()->getPostcode()
        ], $this->getShippingDetails(), $defaultPaymentMethod);
    }

    private function getShippingDetails(): array
    {
        $order = $this->getOrder();
        $shippingDetails = [];

        // @note exclude shipping address on virtual products
        if ($order->getShippingAddress()) {

            $shippingStreet = (array) $order->getShippingAddress()->getStreet();
            $shippingCity = $order->getShippingAddress()->getCity();
            $shippingCountry = $order->getShippingAddress()->getCountryId();

            array_push($shippingStreet, 'N/A', 'N/A');
            list($shippingStreetOne, $shippingStreetTwo) = $shippingStreet;

            $shippingDetails = [
                'shipping.street1' => $shippingStreetOne,
                'shipping.street2' => $shippingStreetTwo,
                'shipping.city' => $shippingCity,
                'shipping.country' => $shippingCountry,
            ];
        }

        return $shippingDetails;
    }
}
