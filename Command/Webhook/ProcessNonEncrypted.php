<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Command\Webhook;

use Magento\Payment\Model\Method\Logger;
use PeachPayments\Hosted\Helper\Data as DataHelper;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

/**
 * Process general webhook
 *
 * @spi
 */
class ProcessNonEncrypted
{
    /**
     * @var DataHelper
     */
    private $dataHelper;
    /**
     * @var WebhooksFactory
     */
    private $webhooksFactory;
    /**
     * @var WebhooksResource
     */
    private $webhooksResource;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;

    /**
     * @param DataHelper $dataHelper
     * @param WebhooksFactory $webhooksFactory
     * @param WebhooksResource $webhooksResource
     * @param Logger $logger
     * @param JsonSerializer $jsonSerializer
     */
    public function __construct(
        DataHelper $dataHelper,
        WebhooksFactory $webhooksFactory,
        WebhooksResource $webhooksResource,
        Logger $logger,
        JsonSerializer $jsonSerializer
    ) {
        $this->dataHelper = $dataHelper;
        $this->webhooksFactory = $webhooksFactory;
        $this->webhooksResource = $webhooksResource;
        $this->logger = $logger;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * Process webhook
     *
     * @param array $data
     * @return void
     */
    public function execute(array $data)
    {
        if (isset($data['customParameters'])) {
            foreach ($data['customParameters'] as $key => $value) {
                $data["customParameters[$key]"] = $value;
            }

            unset($data['customParameters']);
        }

        try {
            $signed = $this->dataHelper->signData($data, false);
            $webhook = $this->webhooksFactory->create();
            $this->webhooksResource->load($webhook, $data['merchantTransactionId'], 'order_increment_id');

            if (!$webhook->getId() || $signed['signature'] !== $data['signature']) {
                // Log errors
                $this->logger->debug(
                    [
                        'message' => 'Error checking request signature.',
                        'webhook_id' => $webhook->getId(),
                        'signature_signed' => $signed['signature'],
                        'signature_in_request' => $data['signature'],
                        'request' => $data
                    ]
                );

                return;
            }

            $insert = $this->mapWebhookData($data);

            foreach ($insert as $key => $item) {
                $webhook->setData($key, $item);
            }

            // save full request
            $webhook->setData('request', $this->jsonSerializer->serialize($insert));
            $this->webhooksResource->save($webhook);
            $this->dataHelper->processOrder($webhook);
        } catch (\Exception $e) {
            $this->logger->debug([
                'message' => 'Error processing Non Encrypted webhook',
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }

    /**
     * Map request data to webhook object keys
     *
     * @param array $data
     * @return array
     */
    private function mapWebhookData(array $data): array
    {
        $result = [];
        foreach ($data as $key => $datum) {
            if ($key === 'id') {
                $result['peach_id'] = $datum;
            } else {
                $result[$this->convertWebhookDataKey($key)] = $datum;
            }
        }

        return $result;
    }

    /**
     * Convert webhook object key
     *
     * @param string $key
     * @return string
     */
    private function convertWebhookDataKey(string $key): string
    {
        return strtolower(
            preg_replace(
                ['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'],
                '$1_$2',
                str_replace('.', '_', $key)
            )
        );
    }
}
