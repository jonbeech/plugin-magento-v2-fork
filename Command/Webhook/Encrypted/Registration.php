<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Command\Webhook\Encrypted;

use PeachPayments\Hosted\Model\Web\Hooks as Webhook;

/**
 * Webhook type registration command
 */
class Registration
{
    /**
     * @param Webhook $webhook
     * @param array $request
     * @return void
     */
    public function process(Webhook $webhook, array $request)
    {
        $webhook->setData('peach_id', $request['payload']['id']);
        $webhook->setData('payment_brand', $request['payload']['paymentBrand']);
        $webhook->setData('result_code', $request['payload']['result']['code']);
        $webhook->setData('result_description', $request['payload']['result']['description']);
    }
}
