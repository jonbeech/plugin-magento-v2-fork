<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Command\Webhook\Encrypted;

use PeachPayments\Hosted\Model\Web\Hooks as Webhook;

/**
 * Webhook type payment command
 */
class Payment
{
    /**
     * @param Webhook $webhook
     * @param array $request
     * @return void
     */
    public function process(Webhook $webhook, array $request)
    {
        $webhook->setData('amount', $request['payload']['amount']);
        $webhook->setData('currency', $request['payload']['currency']);
        $webhook->setData('peach_id', $request['payload']['id']);
        $webhook->setData(
            'merchant_name',
            $request['payload']['customer']['givenName'] .
            $request['payload']['customer']['surname']
        );
        $webhook->setData('payment_brand', $request['payload']['paymentBrand']);
        $webhook->setData('payment_type', $request['payload']['paymentType']);
        $webhook->setData('result_code', $request['payload']['result']['code']);
        $webhook->setData('result_description', $request['payload']['result']['description']);
    }
}
