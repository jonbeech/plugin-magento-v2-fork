<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Command\Webhook;

use Laminas\Http\Headers;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Payment\Model\Method\Logger;
use PeachPayments\Hosted\Command\Webhook\Encrypted\Payment as PaymentCommand;
use PeachPayments\Hosted\Command\Webhook\Encrypted\Registration as RegistrationCommand;
use PeachPayments\Hosted\Command\Webhook\Encrypted\Risk as RiskCommand;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;

/**
 * Process encrypted webhook
 *
 * @spi
 */
class ProcessEncrypted
{
    const REQUEST_BODY_KEY  = 'encryptedBody';
    const HEADER_IV         = 'X-Initialization-Vector';
    const HEADER_AUTH_TAG   = 'X-Authentication-Tag';

    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var WebhooksFactory
     */
    private $webhooksFactory;
    /**
     * @var WebhooksResource
     */
    private $webhooksResource;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;
    /**
     * @var PaymentCommand
     */
    private $paymentCommand;
    /**
     * @var RegistrationCommand
     */
    private $registrationCommand;
    /**
     * @var RiskCommand
     */
    private $riskCommand;

    /**
     * @param ConfigHelper $configHelper
     * @param Logger $logger
     * @param WebhooksFactory $webhooksFactory
     * @param WebhooksResource $webhooksResource
     * @param JsonSerializer $jsonSerializer
     * @param PaymentCommand $paymentCommand
     * @param RegistrationCommand $registrationCommand
     * @param RiskCommand $riskCommand
     */
    public function __construct(
        ConfigHelper $configHelper,
        Logger $logger,
        WebhooksFactory $webhooksFactory,
        WebhooksResource $webhooksResource,
        JsonSerializer $jsonSerializer,
        PaymentCommand $paymentCommand,
        RegistrationCommand $registrationCommand,
        RiskCommand $riskCommand
    ) {
        $this->configHelper = $configHelper;
        $this->logger = $logger;
        $this->webhooksFactory = $webhooksFactory;
        $this->webhooksResource = $webhooksResource;
        $this->jsonSerializer = $jsonSerializer;
        $this->paymentCommand = $paymentCommand;
        $this->registrationCommand = $registrationCommand;
        $this->riskCommand = $riskCommand;
    }

    /**
     * @param string $requestData
     * @param Headers $headers
     * @return void
     */
    public function execute(string $requestData, Headers $headers)
    {
        try {
            $decrypted = $this->decryptRequest($requestData, $headers);

            if (false !== $decrypted) {
                $request = $this->jsonSerializer->unserialize($decrypted);

                if (!isset($request['type'])) {
                    return;
                }

                $webhook = $this->webhooksFactory->create();
                $this->webhooksResource->load($webhook, $request['payload']['id'], 'checkout_id');

                if (!$webhook->getId()) {
                    return;
                }

                switch ($request['type']) {
                    case 'PAYMENT':
                        $this->paymentCommand->process($webhook, $request);
                        break;
                    case 'REGISTRATION':
                        $this->registrationCommand->process($webhook, $request);
                        break;
                    case 'RISK':
                        $this->riskCommand->process($webhook, $request);
                        break;
                }

                $webhook->setData('request', $decrypted);
                $this->webhooksResource->save($webhook);

                $this->logger->debug([
                    'message' => 'Successfully saved Encrypted webhook',
                    'webhook' => $request
                ]);
            }
        } catch (\Exception $e) {
            $this->logger->debug([
                'message' => 'Error processing Encrypted webhook',
                'webhook' => isset($request) ? $request : '',
                'exception' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }

    /**
     * @param string $requestData
     * @param Headers $headers
     * @return false|string
     */
    private function decryptRequest(string $requestData, Headers $headers)
    {
        $content = $this->jsonSerializer->unserialize($requestData);
        $decryptionKey = $this->configHelper->getDecryptionKey();
        $headerIv = $headers->get(self::HEADER_IV)->getFieldValue();
        $headerAuthTag = $headers->get(self::HEADER_AUTH_TAG)->getFieldValue();

        if (isset($content[self::REQUEST_BODY_KEY])) {
            $key = hex2bin($decryptionKey);
            $iv = hex2bin($headerIv);
            $authTag = hex2bin($headerAuthTag);
            $cipherText = hex2bin($content[self::REQUEST_BODY_KEY]);

            return openssl_decrypt(
                $cipherText,
                "aes-256-gcm",
                $key,
                OPENSSL_RAW_DATA,
                $iv,
                $authTag
            );
        }

        return false;
    }
}
