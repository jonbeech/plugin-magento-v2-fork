# Peach Payments Magento payment extension

All-in-one payment solution for African markets. For more information on the various functionality offered by the plugin, see the [Peach Payment documentation hub](https://developer.peachpayments.com/docs/magento).

## Install using Composer

1. Install **Composer** by following the instructions on [https://getcomposer.org/doc/00-intro.md](https://getcomposer.org/doc/00-intro.md).
2. Install the **Peach Payments Magento payment extension**:

   1. Install the plugin by executing `composer require peachpayments/magento2-plugin`.
   2. Enable the payment module by executing:

      ```sh
      php bin/magento module:enable PeachPayments_Hosted
      ```

      ```sh
      php bin/magento setup:upgrade
      ```

   3. If necessary, deploy Magento static content by executing `php bin/magento setup:static-content:deploy`

## Configure the payment extension

1. Log in to your Magento admin dashboard and navigate to **Stores** > **Configuration** > **Sales** > **Payment Methods**.
2. If the Peach Payments module is not visible in the list of available payment methods, go to **System** > **Cache Management** and click **Flush Magento Cache**.
3. Navigate back to **Stores** > **Configuration** > **Sales** > **Payment Methods** and click **Configure** next to the Peach Payments payment method to expand the available settings.
4. Set **Enabled** to **Yes**, configure the correct credentials, specify your preferred settings, and click **Save Config**.

## PHP compatibility

This payment extension supports the `7.3`, `7.4`, and `8.1` PHP versions. PHP 7.1 and 7.2 have been deprecated, so please use version 1.0.7, which excludes GraphQL.

## Events

Two events are available to supplement 3rd-party tracking. On order success, use `peachpayments_order_succeed`. On order failure, use `peachpayments_order_failed`.

Both events have a `result` data object. Take special care to prevent duplicates in your observer. These events are dispatched on both the customer-facing and webhook controllers.

## GraphQL

To support a GraphQA flow, use the core Magento `setPaymentMethodOnCart` and `placeOrder` methods.

After a successful order ID has been returned, use the `getPeachHostedRedirectUrl` method to redirect the customer to
the checkout page (at this stage, the order should be in the pending state). Deconstruct the JSON object from `form_data`
and submit as `_POST` parameters to the specified `form_link` URL.

After the customer has returned to the `return_url` you specified, call the `getPeachHostedOrderStatus` method
to get a success (`1`) or declined (`2`) status. Retry regularly after an interval if the status code is `3`.

### Examples

```graphql

# set peach as payment method
mutation setPaymentMethodOnCart($cartId: String!){
  setPaymentMethodOnCart(input: {
      cart_id: $cartId
      payment_method: {
          code: "peachpayments_hosted_card"
      }
  }) {
    cart {
      selected_payment_method {
        code
      }
    }
  }
}

# place order
mutation placeOrder($cartId: String!) {
  placeOrder(input: {cart_id: $cartId}) {
    order {
      order_number
    }
  }
}

# Get redirect url and data
query getPeachHostedRedirectUrl($cartId: String!){
  getPeachHostedRedirectUrl(input: {
    cart_id: $cartId,
    return_url: "https://my.app.pwa/payment/welcome-back.html"
  }) {
    form_data
    form_link
  }
}

# Get staus
query getPeachHostedOrderStatus($orderId: String!){
  getPeachHostedOrderStatus(input: { order_id: $orderId }) {
    status
  }
}

```

## Subscription support

To support subscriptions, install the [ParadoxLabs Adaptive Subscriptions plugin](https://marketplace.magento.com/paradoxlabs-subscriptions.html). Navigate to **Stores** > **Configuration** > **Sales** > **Payment Methods** > **Peach Payments** > **Server to Server** and enable the server to server integration.

### Compatability matrix

| Magento       | Peach Payments module | ParadoxLabs Subscription module |
|---------------|-----------------------|---------------------------------|
| 2.4.0 - 2.4.3 | Version: 1.2.4        | Version: 3.3.4                  |
| 2.4.4 - 2.4.6 | Version: 1.2.5        | Version: 3.5.2                  |
| 2.4.4 - 2.4.6 | Version: 1.2.6        | Version: 3.5.2                  |
