<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Controller\Secure;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Group;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\UrlInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\GuestCartManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PeachPayments\Hosted\Gateway\Validator\Response as ResponseValidator;
use PeachPayments\Hosted\Model\Api\CopyAndPayCheckPaymentStatus;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;

class CopyAndPayCheck implements HttpGetActionInterface
{
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;
    /**
     * @var UrlInterface
     */
    private $urlBuilder;
    /**
     * @var Http
     */
    private $response;
    /**
     * @var CopyAndPayCheckPaymentStatus
     */
    private $checkPaymentStatus;
    /**
     * @var ResponseValidator
     */
    private $responseValidator;
    /**
     * @var CartManagementInterface
     */
    private $cartManagement;
    /**
     * @var GuestCartManagementInterface
     */
    private $guestCartManagement;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var MessageManagerInterface
     */
    private $messageManager;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;
    /**
     * @var WebhooksFactory
     */
    private $webhooksFactory;
    /**
     * @var WebhooksResource
     */
    private $webhooksResource;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param RequestInterface $request
     * @param CheckoutSession $checkoutSession
     * @param UrlInterface $urlBuilder
     * @param Http $response
     * @param CopyAndPayCheckPaymentStatus $checkPaymentStatus
     * @param ResponseValidator $responseValidator
     * @param CartManagementInterface $cartManagement
     * @param GuestCartManagementInterface $guestCartManagement
     * @param CustomerSession $customerSession
     * @param MessageManagerInterface $messageManager
     * @param JsonSerializer $jsonSerializer
     * @param WebhooksFactory $webhooksFactory
     * @param WebhooksResource $webhooksResource
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        RequestInterface $request,
        CheckoutSession $checkoutSession,
        UrlInterface $urlBuilder,
        Http $response,
        CopyAndPayCheckPaymentStatus $checkPaymentStatus,
        ResponseValidator $responseValidator,
        CartManagementInterface $cartManagement,
        GuestCartManagementInterface $guestCartManagement,
        CustomerSession $customerSession,
        MessageManagerInterface $messageManager,
        JsonSerializer $jsonSerializer,
        WebhooksFactory $webhooksFactory,
        WebhooksResource $webhooksResource,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->request = $request;
        $this->checkoutSession = $checkoutSession;
        $this->urlBuilder = $urlBuilder;
        $this->response = $response;
        $this->checkPaymentStatus = $checkPaymentStatus;
        $this->responseValidator = $responseValidator;
        $this->cartManagement = $cartManagement;
        $this->guestCartManagement = $guestCartManagement;
        $this->customerSession = $customerSession;
        $this->messageManager = $messageManager;
        $this->jsonSerializer = $jsonSerializer;
        $this->webhooksFactory = $webhooksFactory;
        $this->webhooksResource = $webhooksResource;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheirtDoc
     */
    public function execute()
    {
        $id = $this->request->getParam('id');

        if (!$id) {
            return $this->getRedirectToCheckoutBilling();
        }

        try {
            $paymentStatus = $this->checkPaymentStatus->execute($id);
            $validation = $this->responseValidator->validate(['response' => $paymentStatus]);
            $quote = $this->checkoutSession->getQuote();
            $idInQuote = $quote->getPayment()->getAdditionalInformation(AuthorizationTrxIdHandler::KEY_TNX_ID);
            $isAllValid = $validation->isValid() && $id == $idInQuote;
            if (!$isAllValid) {
                return $this->getRedirectToCheckoutBilling();
            }

            $quote->getPayment()->setAdditionalInformation(
                AuthorizationTrxIdHandler::KEY_TNX_ID,
                $paymentStatus['id']
            );
            $quote->getPayment()->setAdditionalInformation(
                AuthorizationTrxIdHandler::KEY_RESPONSE_DETAILS,
                $this->jsonSerializer->serialize($paymentStatus) ?? ''
            );
            $quote->getPayment()->setLastTransId($paymentStatus['id']);

            if (!$this->customerSession->isLoggedIn()) {
                $quote->setCustomerId(null)
                    ->setCustomerEmail($quote->getBillingAddress()->getEmail())
                    ->setCustomerIsGuest(true)
                    ->setCustomerGroupId(
                        Group::NOT_LOGGED_IN_ID
                    );
            }

            $orderId = $this->cartManagement->placeOrder($quote->getId());
            $this->updateWebhook($id, $orderId);
            return $this->response->setRedirect(
                $this->urlBuilder->getUrl('checkout/onepage/success')
            );
        } catch (\Exception $e) {
            return $this->getRedirectToCheckoutBilling();
        }
    }

    /**
     * @return Http|\Magento\Framework\App\Response\HttpInterface
     */
    private function getRedirectToCheckoutBilling()
    {
        $this->messageManager->addErrorMessage('Something is wrong with your payment. Please try again later.');
        return $this->response->setRedirect($this->urlBuilder->getUrl('checkout/cart'));
    }

    /**
     * @param string $checkoutId
     * @param int $orderId
     * @return void
     */
    private function updateWebhook(string $checkoutId, int $orderId)
    {
        try {
            $order = $this->orderRepository->get($orderId);
            $webhook = $this->webhooksFactory->create();
            $this->webhooksResource->load($webhook, $checkoutId, 'checkout_id');
            $webhook->setOrderId($orderId);
            $webhook->setOrderIncrementId($order->getIncrementId());
            $this->webhooksResource->save($webhook);
        } catch (\Exception $e) {
        }
    }
}
