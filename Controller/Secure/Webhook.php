<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

namespace PeachPayments\Hosted\Controller\Secure;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\LayoutFactory;
use PeachPayments\Hosted\Model\Web\HooksFactory;
use PeachPayments\Hosted\Command\Webhook\ProcessNonEncrypted;
use PeachPayments\Hosted\Command\Webhook\ProcessEncrypted;

/**
 * Webhook controller
 */
class Webhook extends AbstractSecure implements CsrfAwareActionInterface
{
    /**
     * @var ProcessNonEncrypted
     */
    private $processNonEncrypted;
    /**
     * @var ProcessEncrypted
     */
    private $processEncrypted;

    /**
     * @param Context $context
     * @param HooksFactory $webHooksFactory
     * @param LayoutFactory $viewLayoutFactory
     * @param ProcessNonEncrypted $processNonEncrypted
     * @param ProcessEncrypted $processEncrypted
     */
    public function __construct(
        Context         $context,
        HooksFactory    $webHooksFactory,
        LayoutFactory   $viewLayoutFactory,
        ProcessNonEncrypted $processNonEncrypted,
        ProcessEncrypted $processEncrypted
    ) {
        $this->processNonEncrypted = $processNonEncrypted;
        $this->processEncrypted = $processEncrypted;
        parent::__construct($context, $webHooksFactory, $viewLayoutFactory);
    }

    /**
     * Webhook action
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $content = $this->getRequest()->getContent();

        if (count($params)) {
            $this->processNonEncrypted->execute($params);
            return $this->getResponse()->setHttpResponseCode(200);
        }

        if (strlen($content) > 0) {
            $this->processEncrypted->execute($content, $this->getRequest()->getHeaders());
            return $this->getResponse()->setHttpResponseCode(200);
        }

        return $this->getResponse()->setHttpResponseCode(200);
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
