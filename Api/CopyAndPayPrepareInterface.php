<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Api;

/**
 * @api
 */
interface CopyAndPayPrepareInterface
{
    /**
     * Create copy and pay checkout
     *
     * @param  string $cartId
     * @return string
     */
    public function execute(string $cartId): string;
}
