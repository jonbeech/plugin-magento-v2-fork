<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Api;

/**
 * @api
 */
interface CopyAndPayPrepareAddNewCartFormInterface
{
    /**
     * Create copy and pay add new cart form
     *
     * @return string checkout id
     */
    public function execute(): string;
}
