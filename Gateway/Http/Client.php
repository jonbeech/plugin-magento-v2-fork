<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Http;

use Magento\Framework\HTTP\Client\Curl as CurlClient;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;

class Client extends CurlClient implements ClientInterface
{
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;

    /**
     * @param Logger $logger
     * @param JsonSerializer $jsonSerializer
     * @param null $sslVersion
     */
    public function __construct(
        Logger         $logger,
        JsonSerializer $jsonSerializer,
        $sslVersion = null
    ) {
        parent::__construct($sslVersion);
        $this->logger = $logger;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @inheritDoc
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        $transferBody = $transferObject->getBody();
        $log = [
            'Request uri' => $transferObject->getUri()
        ];

        $this->setHeaders($transferObject->getHeaders());
        switch ($transferObject->getMethod()) {
            case \Zend_Http_Client::GET:
                $this->get($transferObject->getUri());
                break;
            case \Zend_Http_Client::POST:
                $this->post($transferObject->getUri(), $transferBody);
                break;
            default:
                $this->makeRequest($transferObject->getMethod(), $transferObject->getUri());
        }

        try {
            $response = $this->getBody();
            $log['Response'] = $response;
            $result = $this->jsonSerializer->unserialize($response);
            $this->logger->debug($log);

            return $result;
        } catch (\Exception $exception) {
            $log['Exception'] = $exception->getMessage();
            $this->logger->debug($log);

            throw new ClientException(__($exception->getMessage()));
        }
    }
}
