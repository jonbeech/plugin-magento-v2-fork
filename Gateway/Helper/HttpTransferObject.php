<?php

namespace PeachPayments\Hosted\Gateway\Helper;

use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use Magento\Payment\Gateway\Http\TransferBuilder;

class HttpTransferObject
{
    /**
     * @var TransferBuilder
     */
    private $transferBuilder;
    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * @param TransferBuilder $transferBuilder
     * @param ConfigHelper $configHelper
     */
    public function __construct(
        TransferBuilder $transferBuilder,
        ConfigHelper $configHelper
    ) {
        $this->transferBuilder = $transferBuilder;
        $this->configHelper = $configHelper;
    }

    public function create(
        string $uri,
        string $method,
        array $body = []
    ) {
        return $this->transferBuilder
            ->setUri($uri)
            ->setMethod($method)
            ->setBody($body)
            ->setHeaders(
                [
                    'Authorization' => 'Bearer ' . $this->configHelper->getAccessToken(),
                    CURLOPT_SSL_VERIFYPEER => $this->configHelper->isLiveMode(),
                    CURLOPT_RETURNTRANSFER => true
                ]
            )
            ->build();
    }
}
