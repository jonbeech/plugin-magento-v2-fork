<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Validator;

use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ResultInterface;

class Response extends AbstractValidator
{
    //   Success results codes regexps
    const RESULT_SUCCESS = '/^(000\.000\.|000\.100\.1|000\.[36])/';
    const RESULT_NEED_REVIEW = '/^(000\.400\.0[^3]|000\.400\.[0-1]{2}0)/';
    const RESULT_PENDING = '/^(000\.200)/';
    const RESULT_PENDING_MIGHT_CHANGE = '/^(800\.400\.5|100\.400\.500)/';

    /**
     * @inheritDoc
     */
    public function validate(array $validationSubject)
    {
        if ($this->isSuccess($validationSubject['response'])) {
            return $this->createResult(true);
        }

        return $this->createResult(false);
    }

    /**
     * Check transaction
     *
     * @param array $body
     * @return bool
     */
    private function isSuccess(array $body): bool
    {
        $dummyTransactionResult = ['code' => '', 'description' => 'Something went wrong, check logs.'];
        $result = array_key_exists('result', $body) ? $body['result'] : $dummyTransactionResult;
        $regexps = [
            self::RESULT_SUCCESS,
            self::RESULT_NEED_REVIEW,
            self::RESULT_PENDING,
            self::RESULT_PENDING_MIGHT_CHANGE
        ];
        $matchesCounter = 0;
        $code = $result['code'];

        array_walk($regexps, function ($item) use (&$matchesCounter, $code) {
            preg_match($item, $code, $matches);
            $matchesCounter += count($matches);
        });

        return $matchesCounter > 0;
    }
}
