<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Gateway\Response;

use Magento\Payment\Gateway\Response\HandlerInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;

/**
 * Handle transaction id for PA ( Authorize ) payment action
 * And store it in payment additional information
 */
class AuthorizationTrxIdHandler implements HandlerInterface
{
    const KEY_TNX_ID = 'pp_transaction_id';
    const KEY_RESPONSE_DETAILS = 'pp_response';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @param SubjectReader $subjectReader
     */
    public function __construct(SubjectReader $subjectReader)
    {
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritDoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = $this->subjectReader->readPayment($handlingSubject);
        $payment = $paymentDO->getPayment();
        $transactionId = $response['id'] ?? null;
        if (null !== $transactionId) {
            $payment->setLastTransId($transactionId);
            $payment->setAdditionalInformation(self::KEY_TNX_ID, $transactionId);
        }
    }
}
