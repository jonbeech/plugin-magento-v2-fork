<?php

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;

class RefundDataBuilder implements BuilderInterface
{
    const TYPE = 'RF';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        SubjectReader $subjectReader
    ) {
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        $payment = $this->subjectReader->readPayment($buildSubject);
        $amount = number_format(
            $payment->getPayment()->getCreditMemo()->getGrandTotal(),
            2,
            '.',
            ''
        );

        return [
            PaymentDataBuilder::PAYMENT_TYPE  => self::TYPE,
            PaymentDataBuilder::AMOUNT        => $amount,
            PaymentDataBuilder::CURRENCY      => $payment->getOrder()->getCurrencyCode()
        ];
    }
}
