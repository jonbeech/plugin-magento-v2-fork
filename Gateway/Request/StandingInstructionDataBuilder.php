<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\Module\Manager;

class StandingInstructionDataBuilder implements BuilderInterface
{
    /**
     * @var SubjectReader
     */
    private $subjectReader;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var QuoteManager|null
     */
    private $quoteManager;

    /**
     * @param SubjectReader $subjectReader
     * @param CartRepositoryInterface $cartRepository
     * @param ObjectManagerInterface $objectManager
     * @param Manager $moduleManager
     */
    public function __construct(
        SubjectReader $subjectReader,
        CartRepositoryInterface $cartRepository,
        ObjectManagerInterface $objectManager,
        Manager $moduleManager
    ) {
        $this->subjectReader = $subjectReader;
        $this->cartRepository = $cartRepository;

        // Backwards compatibility for PHP versions which do not support Nullable Types
        if ($moduleManager->isEnabled('ParadoxLabs_Subscriptions')) {
            $this->quoteManager = $objectManager->get('ParadoxLabs\Subscriptions\Model\Service\QuoteManager');
        }
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        if ($this->quoteManager !== null) {
            try {
                return $this->getStandingInstructions($buildSubject);
            } catch (NoSuchEntityException $noSuchEntityException) {
                return [];
            }
        }

        return [];
    }

    /**
     * @param array $buildSubject
     * @return array|string[]
     * @throws NoSuchEntityException
     */
    private function getStandingInstructions(array $buildSubject): array
    {
        $payment = $this->subjectReader->readPayment($buildSubject)->getPayment();
        $order = $payment->getOrder();
        $transactionId = (string) $payment->getAdditionalInformation(AuthorizationTrxIdHandler::KEY_TNX_ID);
        /** @var OrderItemInterface $item */
        $quote = $this->cartRepository->get($order->getQuoteId());
        $hasSubscription = $this->quoteManager->quoteContainsSubscription($quote);

        if ($hasSubscription) {
            if ($transactionId) {
                return [
                    'standingInstruction.mode' => 'REPEATED',
                    'standingInstruction.type' => 'RECURRING',
                    'standingInstruction.source' => 'MIT',
                ];
            }

            return [
                'standingInstruction.mode'   => 'INITIAL',
                'standingInstruction.type'   => 'RECURRING',
                'standingInstruction.source' => 'CIT'
            ];
        }

        return [];
    }
}
