<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;

class VaultDataBuilder implements BuilderInterface
{
    const MODE      = 'standingInstruction.mode';
    const TYPE      = 'standingInstruction.type';
    const SOURCE    = 'standingInstruction.source';
    const VAULT_ON  = 'createRegistration';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @param SubjectReader $subjectReader
     */
    public function __construct(SubjectReader $subjectReader)
    {
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        $payment = $this->subjectReader->readPayment($buildSubject)->getPayment();
        $isStoreCardDetails = $payment->getAdditionalInformation('is_active_payment_token_enabler');

        if ($isStoreCardDetails) {
            return [
                self::VAULT_ON  => true,
                self::MODE      => 'INITIAL',
                self::TYPE      => 'UNSCHEDULED',
                self::SOURCE    => 'CIT'
            ];
        }

        return [];
    }
}
