<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;

class PaymentDataBuilder implements BuilderInterface
{
    const PAYMENT_TYPE  = 'paymentType';
    const AMOUNT        = 'amount';
    const CURRENCY      = 'currency';

    /**
     * @var SubjectReader
     */
    private $subjectReader;
    /**
     * @var string
     */
    private $paymentType;

    /**
     * @param SubjectReader $subjectReader
     * @param string $paymentType
     */
    public function __construct(
        SubjectReader $subjectReader,
        string $paymentType = 'PA'
    ) {
        $this->subjectReader = $subjectReader;
        $this->paymentType = $paymentType;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        $payment = $this->subjectReader->readPayment($buildSubject);
        $amount = number_format(
            $payment->getPayment()->getAmountOrdered(),
            2,
            '.',
            ''
        );

        return [
            self::PAYMENT_TYPE  => $this->paymentType,
            self::AMOUNT        => $amount,
            self::CURRENCY      => $payment->getOrder()->getCurrencyCode()
        ];
    }
}
