<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway;

use Magento\Payment\Gateway\CommandInterface;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PeachPayments\Hosted\Gateway\Response\VaultTokenHandler;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

class CopyAndPaySale implements CommandInterface
{
    /**
     * @var VaultTokenHandler
     */
    private $vaultTokenHandler;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;

    /**
     * @param VaultTokenHandler $vaultTokenHandler
     * @param JsonSerializer $jsonSerializer
     */
    public function __construct(
        VaultTokenHandler $vaultTokenHandler,
        JsonSerializer $jsonSerializer
    ) {
        $this->vaultTokenHandler = $vaultTokenHandler;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @inheritDoc
     */
    public function execute(array $commandSubject)
    {
        $payment = $commandSubject["payment"]->getPayment();
        $trxId = $payment->getAdditionalInformation(AuthorizationTrxIdHandler::KEY_TNX_ID);
        $response = $payment->getAdditionalInformation(AuthorizationTrxIdHandler::KEY_RESPONSE_DETAILS);

        if ($response) {
            $response = $this->jsonSerializer->unserialize($response);
        }

        if (isset($response['registrationId'])) {
            $this->vaultTokenHandler->handle($commandSubject, $response);
        }

        $payment->setTransactionId($trxId);
    }
}
