<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHostedHelper;

class Config extends AbstractHelper
{
    const HOST_LIVE = 'https://eu-prod.oppwa.com/';
    const HOST_TEST = 'https://eu-test.oppwa.com/';
    const CC_BRANDS = [
        'VI' => ['code' => 'VISA', 'label' => 'Visa'],
        'MC' => ['code' => 'MASTER', 'label' => 'MasterCard']
    ];


    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            PeachPaymentsHostedHelper::XML_CONF . 'peachpayments_server_to_server/active',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function isOnlyForSubscription()
    {
        return $this->scopeConfig->isSetFlag(
            PeachPaymentsHostedHelper::XML_CONF . 'peachpayments_server_to_server/subs_only',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->scopeConfig->getValue(
            PeachPaymentsHostedHelper::XML_CONF . 'token',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    private function getApiUrl()
    {
        return $this->isLiveMode() ? self::HOST_LIVE : self::HOST_TEST;
    }

    /**
     * @param string $registrationId
     * @return string
     */
    public function getApiRegistrationUri(string $registrationId)
    {
        return sprintf('%sv1/registrations/%s/payments', $this->getApiUrl(), $registrationId);
    }

    /**
     * @param string $registrationId
     * @param string $entityId
     * @return string
     */
    public function getApiDeleteRegistrationUri(string $registrationId, string $entityId)
    {
        return sprintf('%sv1/registrations/%s?entityId=%s', $this->getApiUrl(), $registrationId, $entityId);
    }

    /**
     * @param string|null $authorizationTrxId
     * @return string
     */
    public function getApiPaymentsUri(string $authorizationTrxId = null)
    {
        if (null !== $authorizationTrxId) {
            $authorizationTrxId = '/' . $authorizationTrxId;
        }

        return sprintf('%sv1/payments%s', $this->getApiUrl(), $authorizationTrxId ?? '');
    }

    /**
     * @param string $code
     * @return string
     */
    public function getPaymentBrandByPeachPaymentsCode(string $code)
    {
        foreach (self::CC_BRANDS as $brand => $data) {
            if ($data['code'] == $code) {
                return $brand;
            }
        }

        return '';
    }

    /**
     * @return bool
     */
    public function isLiveMode()
    {
        return $this->scopeConfig->isSetFlag(PeachPaymentsHostedHelper::XML_CONF . 'mode', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     * TODO: change to one method
     */
    public function getEntityId3DSecure()
    {
        if ($this->isLiveMode()) {
            return $this->scopeConfig->getValue(
                PeachPaymentsHostedHelper::XML_CONF . 'entity_id_theed_secure',
                ScopeInterface::SCOPE_STORE
            );
        }

        return $this->scopeConfig->getValue(
            PeachPaymentsHostedHelper::XML_CONF . 'entity_id_theed_secure_sandbox',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param string $id
     * @return string
     */
    public function getCheckoutsUri(string $id = '')
    {
        return $id ? $this->getApiUrl() . 'v1/checkouts/' . $id . '/payment' :
            $this->getApiUrl() . 'v1/checkouts';
    }

    /**
     * @return string
     */
    public function getDecryptionKey()
    {
        return (string) $this->scopeConfig->getValue(PeachPaymentsHostedHelper::XML_CONF . 'webhook_decryption_key');
    }

    /**
     * @return string
     */
    public function getPaymentFormUrl(): string
    {
        return $this->getApiUrl() . 'v1/paymentWidgets.js?checkoutId=';
    }

    /**
     * @see https://developer.peachpayments.com/reference/post_checkout
     *
     * @param string $code
     * @return string
     */
    public function getIsForceDefaultMethod(string $code): string
    {
        $selected = (string) $this->scopeConfig->getValue('payment/peachpayments_hosted/force_default_methods');

        if (strpos(strtolower($selected), strtolower($code)) !== false) {
            return 'true';
        }

        return 'false';
    }
}
